package ru.t1.azarin.tm.api.model;

import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;

public interface ICommand {

    void execute() throws SQLException;

    @Nullable
    String getName();

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

}
