package ru.t1.azarin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.AbstractModelDTO;

public interface IServiceDTO<M extends AbstractModelDTO> {

    void add(@Nullable M model);

    void remove(@Nullable M model);

    void update(@Nullable M model);

}
