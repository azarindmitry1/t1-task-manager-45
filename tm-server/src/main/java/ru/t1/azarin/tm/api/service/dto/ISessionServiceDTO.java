package ru.t1.azarin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionServiceDTO {

    void add(@Nullable SessionDTO model);

    void clear(@Nullable String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<SessionDTO> findAll(@Nullable String userId);

    @Nullable
    SessionDTO findOneById(@Nullable String userId, @Nullable String id);

    void remove(@Nullable SessionDTO model);

    void removeById(@Nullable String userId, @Nullable String id);

    void update(@Nullable SessionDTO model);

}
