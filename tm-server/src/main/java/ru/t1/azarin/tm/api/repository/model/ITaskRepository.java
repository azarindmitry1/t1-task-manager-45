package ru.t1.azarin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Nullable
    List<Task> findAll(@NotNull String userId, @NotNull Sort sort);

    @Nullable
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}