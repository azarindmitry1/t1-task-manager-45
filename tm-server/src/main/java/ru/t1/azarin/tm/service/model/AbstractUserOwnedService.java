package ru.t1.azarin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.azarin.tm.api.service.IConnectionService;
import ru.t1.azarin.tm.api.service.model.IUserOwnedService;
import ru.t1.azarin.tm.model.AbstractUserOwnedModel;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}
